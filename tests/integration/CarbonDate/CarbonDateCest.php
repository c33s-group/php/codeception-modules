<?php

declare(strict_types=1);

namespace Tests\Integration\CarbonDate;

use C33s\Codeception\Module\CarbonDate;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use DateTimeImmutable;
use Tests\IntegrationTester;

/**
 * @group carbon_date
 * @group carbondate
 */
final class CarbonDateCest
{
    public function _after(IntegrationTester $I): void
    {
        $I->stopSimulatingCurrentDate();
    }

    public function CarbonDateAndDateAreEqualTest(IntegrationTester $I): void
    {
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();

        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
    }

    public function SimulatingDateChangesCarbonDateNow(IntegrationTester $I): void
    {
        $I->simulateCurrentDateAs('2010-05-10 14:00:00');
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();

        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
        $I->assertEquals('2010-05-10 14:00:00', $carbonNow);
        $I->assertEquals('2010-05-10 14:00:00', $carbonImmutableNow);
    }

    public function StopSimulatingDateWorks(IntegrationTester $I): void
    {
        $I->simulateCurrentDateAs('2010-05-10 14:00:00');
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();

        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
        $I->assertEquals('2010-05-10 14:00:00', $carbonNow);
        $I->assertEquals('2010-05-10 14:00:00', $carbonImmutableNow);

        $I->stopSimulatingCurrentDate();
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();
        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
    }

    /**
     * @prepare disableDateSimulateForCarbon
     */
    public function DateSimulationForCarbonCanBeDisabledWithModuleConfig(IntegrationTester $I): void
    {
        $I->simulateCurrentDateAs('2010-05-10 14:00:00');
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();

        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
        $I->assertNotEquals('2010-05-10 14:00:00', $carbonNow);
        $I->assertEquals('2010-05-10 14:00:00', $carbonImmutableNow);
    }

    /**
     * @group abc
     * @prepare disableDateSimulateForCarbonImmutable
     */
    public function DateSimulationForCarbonImmutableCanBeDisabledWithModuleConfig(IntegrationTester $I): void
    {
        $I->simulateCurrentDateAs('2010-05-10 14:00:00');
        $realNow = new DateTimeImmutable();
        $carbonNow = new Carbon();
        $carbonImmutableNow = new CarbonImmutable();

        $I->assertNotEquals($realNow->format('Y-m-d H:i'), $carbonNow->format('Y-m-d H:i'));
        $I->assertEquals($realNow->format('Y-m-d H:i'), $carbonImmutableNow->format('Y-m-d H:i'));
        $I->assertEquals('2010-05-10 14:00:00', $carbonNow);
        $I->assertNotEquals('2010-05-10 14:00:00', $carbonImmutableNow);
    }

    protected function disableDateSimulateForCarbon(CarbonDate $carbonDateModule): void
    {
        $carbonDateModule->_reconfigure(['simulate_carbon' => false]);
    }

    protected function disableDateSimulateForCarbonImmutable(CarbonDate $carbonDateModule): void
    {
        $carbonDateModule->_reconfigure(['simulate_carbon_immutable' => false]);
    }
}
