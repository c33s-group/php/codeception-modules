<?php

declare(strict_types=1);

namespace C33s\Codeception\Traits;

use Codeception\Lib\Connector\Symfony as SymfonyConnector;
use Codeception\Module;
use Codeception\Module\Symfony;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\InMemoryUser;

trait SymfonyModuleTrait
{
    /**
     * @return Module
     */
    abstract protected function getModule(string $name);

    abstract protected function assertEquals($expected, $actual, string $message = '');

    abstract protected function assertSame($expected, $actual, string $message = '');

    abstract protected function assertNotEquals($expected, $actual, string $message = '');

    /**
     * @param class-string|object $entityClass
     */
    public function getRepository($entityClass): ObjectRepository
    {
        if (is_object($entityClass)) {
            $entityClass = get_class($entityClass);
        }

        return $this->getEntityManager()->getRepository($entityClass);
    }

    /**
     * Sets the Host which will be simulated to access. This is necessary to access routes which only listen to a
     * specific host.
     * If not set `localhost` will be used.
     */
    public function setHost(string $host): void
    {
        $this->getSymfonyClient()->setServerParameter('HTTP_HOST', $host);
    }

    /**
     * Auto Login a User without creating it in the Database with writing directly to session.
     * doesn't work with Groups.
     *
     * @param string[] $roles
     *
     * @see https://stackoverflow.com/questions/9550079/how-to-programmatically-login-authenticate-a-user
     * @see https://stackoverflow.com/a/70987764
     * @see https://github.com/symfony/symfony/issues/40662
     */
    public function loggedInAs(
        string $username = 'admin',
        string $firewallContext = 'main',
        array $roles = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER', 'ROLE_SONATA_ADMIN']
    ): void {
        $session = $this->getSession();
        $client = $this->getSymfonyClient();
        $user = new InMemoryUser(
            $username,
            null,
            $roles,
            true,
            true,
            true,
            true,
            []
        );
        //for legacy symfony versions:
        //$token = new UsernamePasswordToken($username, $password = null, $firewallContext, $roles);

        $token = new UsernamePasswordToken($user, $firewallContext, $roles);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }

    public function loggedInAsAdmin(): void
    {
        $this->loggedInAs('admin', 'main', ['ROLE_ADMIN']);
    }

    public function loggedInAsEditor(): void
    {
        $this->loggedInAs('editor', 'main', ['ROLE_EDITOR']);
    }

    /**
     * Checks if the last url access was a redirect to the $url given in this method. To check if also the correct
     * status code was sent, provide it by $expectedStatusCode.
     *
     * Redirects have to be off for this working '$I->followRedirects(false)' before the first url is visited.
     * ```
     * <?php
     * $I->followRedirects(false);
     * $I->amOnPage('/admin/dashboard');
     * $I->seeRedirectTo('/admin/login');
     * ?>
     * ```
     *
     * @see https://github.com/gamajo/codeception-redirects/blob/develop/src/Redirects.php
     */
    public function seeRedirectTo(string $url, ?int $expectedStatusCode = null): void
    {
        $client = $this->getNonRedirectingClient();
        $targetLocation = $client->getResponse()->headers->get('location');
        if ($expectedStatusCode) {
            $this->assertEquals($expectedStatusCode, $client->getResponse()->getStatusCode());
        }
        $this->assertSame(0, stripos(strrev($targetLocation), strrev($url)));
    }

    /**
     * @see Functional::seeRedirectTo
     */
    public function seeTemporaryRedirectTo(string $url): void
    {
        $this->seeRedirectTo($url, 302);
    }

    /**
     * @see Functional::seeRedirectTo
     */
    public function seePermanentRedirectTo(string $url): void
    {
        $this->seeRedirectTo($url, 301);
    }

    /**
     * Ensures that no redirect has taken place after accessing a url.
     */
    public function haveNoRedirect(): void
    {
        $client = $this->getNonRedirectingClient();
        $this->assertEquals(false, $client->getResponse()->headers->get('location', 'false'));
        $this->assertNotEquals(301, $client->getResponse()->getStatusCode());
        $this->assertNotEquals(302, $client->getResponse()->getStatusCode());
    }

    /**
     * Toggle redirections on and off.
     *
     * By default, BrowserKit will follow redirections, so to check for 30*
     * HTTP status codes and Location headers, they have to be turned off.
     *
     * @param bool $followRedirects Optional. Whether to follow redirects or not.
     *                              Default is true.
     */
    public function followRedirects(bool $followRedirects = true): void
    {
        $client = $this->getSymfonyClient();
        $client->followRedirects($followRedirects);
        $client->followMetaRefresh($followRedirects);
    }

    public function reload(): void
    {
        $client = $this->getSymfonyClient();
        $client->reload();
    }

    /**
     * Returns the Symfony Container.
     */
    private function getContainer(): ContainerInterface
    {
        return $this->getSymfonyModule()->_getContainer();
    }

    /**
     * Returns the Codeception Symfony Module.
     */
    private function getSymfonyModule(): Symfony
    {
        /**
         * @var Symfony
         */
        $symfonyModule = $this->getModule('Symfony');

        return $symfonyModule;
    }

    /**
     * Returns the Special SymfonyConnector (\Symfony\Component\HttpKernel\Client).
     *
     * @see Symfony::_before() there `$this->client` gets overwritten with
     *      `= new SymfonyConnector($this->kernel, ...);`
     */
    private function getSymfonyClient(): SymfonyConnector
    {
        return $this->getSymfonyModule()->client;
    }

    private function getNonRedirectingClient(): SymfonyConnector
    {
        $client = $this->getSymfonyModule()->client;
        if ($client->isFollowingRedirects()) {
            $client->followRedirects(false);
            $client->followMetaRefresh(false);
        }

        return $client;
    }

    private function getEntityManager(): EntityManagerInterface
    {
        /** @var EntityManagerInterface $doctrine */
        $doctrine = $this->getContainer()->get('doctrine');

        return $doctrine;
    }

    private function getSession(): Session
    {
        /** @var Session $session */
        $session = $this->getContainer()->get('session');

        return $session;
    }
}
