<?php

declare(strict_types=1);

namespace C33s\Codeception\Module;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Closure;
use Codeception\Module;
use DateTimeInterface;

final class CarbonDate extends Module
{
    /**
     * @var array<string>
     */
    protected $requiredFields = [];

    /**
     * @var array<mixed>
     */
    protected $config = [
        'simulate_carbon' => true,
        'simulate_carbon_immutable' => true,
    ];

    /**
     * @param Closure|DateTimeInterface|string|false|null $dateToSimulate
     *
     * @see https://carbon.nesbot.com/docs/ (Testing Aids)
     */
    public function simulateCurrentDateAs($dateToSimulate): void
    {
        if (true === $this->config['simulate_carbon']) {
            Carbon::setTestNow($dateToSimulate);
        }
        if (true === $this->config['simulate_carbon_immutable']) {
            CarbonImmutable::setTestNow($dateToSimulate);
        }
    }

    public function stopSimulatingCurrentDate(): void
    {
        if ($this->config['simulate_carbon']) {
            Carbon::setTestNow();
        }

        if ($this->config['simulate_carbon_immutable']) {
            CarbonImmutable::setTestNow();
        }
    }
}
