<?php

declare(strict_types=1);

namespace C33s\Codeception\Module;

use C33s\Codeception\Traits\SymfonyModuleTrait;
use Codeception\Module\Symfony as BaseSymfony;

/**
 * Simple wrapper around the Symfony Module Trait which is meant.
 */
final class Symfony extends BaseSymfony
{
    use SymfonyModuleTrait;
}
